import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';

function App() {
    const [data, setData] = useState(null);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        (async () => {
            console.log("setLoading true");
            setLoading(true);
            const rawData = await fetch(`/data?search=${search}`, { headers: { Accept: 'application/json' } });
            {
                setData(await rawData.json());
                setLoading(false);
                console.log("setloading FALSE");
            }
        })();
    }, [search]);

    return (
        <div>
            <input
                type="text"
                value={keyword}
                onChange={event => setKeyword(event.target.value)}
            />
            <button type="button" onClick={() => setSearch(keyword)}>
                Search
          </button>


            {data && !loading &&
                <div className="App">
                    {data.items.length} results.
                    <table>
                        <thead><tr>
                            {data.headers.map((h) => (<th>{h}</th>))}
                        </tr>
                        </thead>
                        <tbody>
                            {data.items.map((row) => (<tr>
                                {row.map((val) => (<td>{val}</td>))}
                            </tr>))}
                        </tbody>

                    </table>

                </div>
            }

            {loading &&
                <div className="App">
                    <em>Loading...</em>
                </div>}
        </div>
    );

}

export default App;

