import unittest
import main
import json
import pprint

class MainTest(unittest.TestCase):
    def setUp(self):
        self.client = main.app.test_client(use_cookies=False)

        # For real unit tests, generate/insert known test data here


    def test_get_date(self):
        rv = self.client.get('/data?search=coagulation')
        assert rv.status_code == 200
        j = json.loads(rv.data)

        assert len(j['items']) == 85
        assert len(j['headers']) == 220

