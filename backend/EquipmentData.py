import csv
import unittest
import pprint
import re
import pickle
from collections import defaultdict

HEADERS = '$HEADERS$'
INDEX_FILENAME = 'sample_data_index.pickle'

class EquipmentData():
    def __init__(self, datastore):
        self.datastore = datastore
                    
    def get_list(self, args):

        search = args.get('search')

        if not search or len(search) < 4:
            return [], []

        data = self.datastore.get_data()

        return self.datastore.get_headers(), self.datastore.get_data()[search.lower()]


