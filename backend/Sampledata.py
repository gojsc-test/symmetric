import csv
import re
import pickle
from collections import defaultdict

INDEX_FILENAME = 'sample_data_index.pickle'
HEADERS_KEY = '$HEADERS$'

class Sampledata():
    def __init__(self):
        self.data = defaultdict(list)      # keys are words and values are the rows that have that word

        try:
            self.data = pickle.load( open(INDEX_FILENAME, 'rb') )
        except:
            with open('../sample_data.csv') as fh:
                print("Rebuilding index...")
                csvreader = csv.reader(fh)
                cnt = 0
                
                have_headers = False

                for row in csvreader:
                    if not have_headers:
                        data[self.HEADERS_KEY] = row
                        have_headers = True
                        continue

                    # Split row in to "words" (4 or more subsequent alpha chars)

                    for word in re.findall(r'[A-Za-z]{4,}', str(row)):
                        word = word.lower()
                        if row not in self.data[word]:
                            self.data[word].append(row)
                    cnt += 1
                    if cnt % 100 == 0:
                        pickle.dump(self.data, open(INDEX_FILENAME, 'wb'))
                        print(cnt, len(self.data.keys()), len(pickle.dumps(self.data)))

    def get_headers(self):
        return self.data[HEADERS_KEY]

    def get_data(self):
        return self.data
                    

