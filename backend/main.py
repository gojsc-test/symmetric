from flask import Flask, jsonify, request
import Sampledata

import EquipmentData

app = Flask(__name__)

@app.before_first_request
def before_first_request():
    """
    Called only once upon instantiation.
    """

    app.sampledata = Sampledata.Sampledata()



@app.route('/data', methods=['GET'])
def data_get():
    equip_data = EquipmentData.EquipmentData(app.sampledata)
    headers, items = equip_data.get_list(request.args)
    return jsonify({"status": "ok",
                    "headers": headers,
                    "items": items})

if __name__ == '__main__':
    app.run()
